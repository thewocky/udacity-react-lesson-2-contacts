import React from 'react';
const girls = [
  {
    "name": "Ellen"
  },
  {
    "name": "Madeline"
  },
  {
    "name": "Mahnaz"
  },
  {
    "name": "Tara"
  },
  {
    "name": "Jill"
  },
  {
    "name": "Lynn"
  },
  {
    "name": "Michelle"
  }
];


function shuffle(array) {
  var currentIndex = array.length, temporaryValue, randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}

class SecretSanta extends React.Component {
	render() {
		const theGirls = shuffle( girls );
		theGirls.forEach((a, index) => {
			const toGirl = index === theGirls.length - 1 ? 0 : index+1;
			a.to = toGirl
	// 		const toGirl = index === theGirls.length - 1 ? theGirls[0] : theGirls[index+1];
	// 		will this also?
			console.log( a.name, toGirl );
		});
		return (
			<ol className='contact-list'>
			{
				theGirls.map((g) => (
				<li key={g.name} className='contact-list-item'>
					<div className='contact-details'>
						<p>{g.name} to {theGirls[g.to].name}</p>
					</div>
				</li>
				))
			}
			</ol>
		)
	}
}
export default SecretSanta;